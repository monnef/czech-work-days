CzechWorkDays
=============
Eta library for testing and counting workdays in Czech Republic.

Limitations
-----------
Only considers current holidays, even for distant past dates. So it is not recommended to use this library for dates older than few years in the past.

Dependencies
------------
* `time` library

Example
=======
You can try this library (e.g. in REPL, the command is `etlas repl`):

```haskell
import Data.Time (fromGregorian)
import CzechWorkDays

...

d = fromGregorian 2018 1 1 -- construct Day
isWorkDay d -- False
getHoliday d -- Just "Nový rok"
countOfWorkdaysInMonth 2018 7 -- 20
```


Development
===========

Tests
-----

```sh
etlas test
```

